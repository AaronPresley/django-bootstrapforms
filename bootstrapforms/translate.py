from .fieldoutput import BSFFieldOutput

class BSFTranslateForm(object):
    form_obj = None

    def __init__(self, form_obj):
        self.form_obj = form_obj

    def form_errors(self):
        output = ""
        errors = self.form_obj.errors.get('__all__')

        if not errors:
            return output

        output = "- ".join(errors)
        return """<div class="alert alert-danger">%s</div>""" % output

    def output(self):
        output = ""

        output += self.form_errors()

        for field in self.form_obj:
            bstrans = BSFTranslateField(field)
            output += bstrans.form_group()
        return output

class BSFTranslateField(object):
    field_obj = None

    def __init__(self, *args, **kwargs):
        self.field_obj = args[0]

        def_value = self.field_obj.field.initial or ""
        data_value = self.field_obj.value() or ""

        self.field_attrs = {
            'id': self.field_obj.html_initial_id,
            'name': self.field_obj.html_name,
            'label': self.field_obj.label,
            'class_string': self.field_obj.field.widget.attrs.get('class', ""),
            'value': data_value or def_value
        }

    def form_group(self):
        return """<div class="form-group">%s%s%s%s</div>""" % (self.label(),
            self.input(), self.help_text(), self.alert_span())

    def label(self, class_string=None):
        return BSFFieldOutput.label(text=self.field_attrs.get('label'),
            class_string=class_string)

    def input(self):
        field_class = class_name = self.field_obj.field.widget.__class__.__name__

        if field_class == "DateTimeInput":
            return BSFFieldOutput.text(type="datetime", **self.field_attrs)

        elif field_class == "EmailInput":
			return BSFFieldOutput.text(type="email", **self.field_attrs)

        elif field_class == "NumberInput":
			return BSFFieldOutput.text(type="number", **self.field_attrs)

        elif field_class == "ClearableFileInput":
            return BSFFieldOutput.fileinput(**self.field_attrs)

        elif field_class == "PasswordInput":
            return BSFFieldOutput.text(type="password", **self.field_attrs)

        elif field_class == "Select":
            options = self.field_obj.field.choices
            return BSFFieldOutput.select(options=options, **self.field_attrs)

        elif field_class == "SelectMultiple":
            options = self.field_obj.field.choices
            return BSFFieldOutput.select(options=options, is_multiple=True,
                **self.field_attrs)

        elif field_class == "TextInput":
            return BSFFieldOutput.text(type="text", **self.field_attrs)

        elif field_class == "Textarea":
            return BSFFieldOutput.textarea(**self.field_attrs)

        elif field_class == "URLInput":
            return BSFFieldOutput.text(type="url", **self.field_attrs)

        else:
            raise Exception("Bootstrap Forms doesn't know what to do with a "
                            "%s input class" % field_class)

    def alert_span(self):
        return BSFFieldOutput.alert_span(type="danger",
            messages=self.field_obj.errors)

    def help_text(self):
        return BSFFieldOutput.help_text(text=self.field_obj.help_text)

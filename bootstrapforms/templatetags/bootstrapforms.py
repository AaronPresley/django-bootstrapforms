from django import template
from django.utils.safestring import mark_safe
from ..translate import BSFTranslateField, BSFTranslateForm

register = template.Library()


@register.simple_tag
def bsform(form):
    bstrans = BSFTranslateForm(form)
    return mark_safe(bstrans.output())

@register.simple_tag
def bsformerror(form):
    bstrans = BSFTranslateForm(form)
    return mark_safe(bstrans.form_errors())

@register.simple_tag
def bsformgroup(field):
    bstrans = BSFTranslateField(field)
    return mark_safe(bstrans.form_group())


@register.simple_tag
def bslabel(field, class_string=None):
    bstrans = BSFTranslateField(field)
    return mark_safe(bstrans.label(class_string))


@register.simple_tag
def bscontrol(field):
    bstrans = BSFTranslateField(field)
    return mark_safe(bstrans.input())


@register.simple_tag
def bsfielderror(field):
    bstrans = BSFTranslateField(field)
    return mark_safe(bstrans.alert_span())

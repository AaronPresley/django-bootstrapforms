
class BSFFieldOutput(object):
    def __init__(self):
        pass

    @staticmethod
    def alert_div(*args, **kwargs):
        """ Outputs a generic alert div
        """

        messages = kwargs.get("messages")
        type = kwargs.get("type", "danger")

        if type(messages) == str:
            messages = [messages]

        output_html += """<div class="alert alert-%s">""" % type
        for msg in messages:
            output_html += "&bull; %s<br/>" % msg
        output_html += """</div>"""

        return output_html

    @staticmethod
    def alert_span(*args, **kwargs):
        """ Outputs a generic alert span
        """

        messages = kwargs.get("messages")
        alert_type = kwargs.get("type", "danger")

        if type(messages) == 'str':
            messages = [messages]

        if len(messages) == 0:
            return ""

        output_html = """<span class="text-%s">""" % alert_type
        output_html += ", ".join(messages)
        output_html += """</span>"""

        return output_html

    @staticmethod
    def fileinput(*args, **kwargs):
        id = kwargs.get('id', "")
        name = kwargs.get("name", "")
        value = kwargs.get("value", "")
        class_string = kwargs.get("class_string", "")
        is_disabled = kwargs.get("is_disabled", False)

        disabled_string = "disabled" if is_disabled else ""

        return """<input type="file" id="%s" name="%s" %s>""" % (id, name,
            disabled_string)

    @staticmethod
    def label(*args, **kwargs):
        class_string = kwargs.get('class_string', None)
        text = kwargs.pop("text", "NO LABEL")

        return """<label class="control-label %s">%s</label>""" % (
            class_string, text)

    @staticmethod
    def text(*args, **kwargs):
        """ Outputs a Bootstrap-ready input field for text input
        """

        id = kwargs.get('id', "")
        name = kwargs.get("name", "")
        value = kwargs.get("value", "")
        type = kwargs.get("type", "text")
        class_string = kwargs.get("class_string", "")
        is_disabled = kwargs.get("is_disabled", False)

        disabled_string = "disabled" if is_disabled else ""

        return """<input type="%s" id="%s" name="%s" value="%s"
            class="form-control %s" %s />""" % (type, id, name, value,
            class_string, disabled_string)

    @staticmethod
    def textarea(*args, **kwargs):
        """ Outputs a Textarea input
        """

        id = kwargs.get('id', "")
        name = kwargs.get("name", "")
        value = kwargs.get("value", "")
        class_string = kwargs.get("class_string", "")
        is_disabled = kwargs.get("is_disabled", False)

        disabled_string = "disabled" if is_disabled else ""

        return """<textarea id="%s" name="%s" class="form-control %s"
            %s>%s</textarea>""" % (id, name, class_string, disabled_string,
            value)

    @staticmethod
    def radio(*args, **kwargs):
        return BSFFieldOutput.__choice_input("radio", *args, **kwargs)

    @staticmethod
    def checkbox(*args, **kwargs):
        return BSFFieldOutput.__choice_input("checkbox", *args, **kwargs)

    @staticmethod
    def select(*args, **kwargs):
        """ Outputs a Select input, currently doesn't support opt groups
        """

        id = kwargs.get("id", "")
        name = kwargs.get("name", "")
        value = kwargs.get("value", "")
        class_string = kwargs.get("class_string", "")
        options = kwargs.get("options", [])
        is_disabled = kwargs.get("is_disabled", False)
        is_multiple = kwargs.get("is_multiple", False)

        disabled_string = "disabled" if is_disabled else ""
        multiple_string = "multiple" if is_multiple else ""

        # Converting the value to always be in a list for easier checking
        # when this is a multiple select
        if type(value) == 'str':
            value = [value]

        option_string = ""
        for opt in options:
            sel_string = "selected" if value and str(opt[0]) in value else ""
            option_string += """<option value="%s" %s>%s</option>""" %  (
                opt[0], sel_string, opt[1])

        return """<select id="%s" name="%s" class="form-control %s" %s %s>%s
            </select> """ % (id, name, class_string, disabled_string,
            multiple_string, option_string)

    @staticmethod
    def help_text(*args, **kwargs):
        text = kwargs.get("text")
        if not text:
            return ""
        return """<p class="help-block">%s</p>""" % text

    # ----- PRIVATE METHODS ------

    @staticmethod
    def __choice_input(*args, **kwargs):
        """ Outputs a SINGLE checkbox or a radio input
        """

        type = kwargs.get("type")
        id = kwargs.get('id', "")
        name = kwargs.get("name", "")
        value = kwargs.get("value", "")
        class_string = kwargs.get("class_string", "")
        is_disabled = kwargs.get("is_disabled", False)

        container_class = "checkbox" if type == "checkbox" else "radio"
        disabled_string = "disabled" if is_disabled else None

        return """<div class="%s"><label><input type="%s" id="%s" name="%s"
            value="%s" class="%s" />%s</label></div>""" % (container_class,
            type, id, name, value, class_string, label)

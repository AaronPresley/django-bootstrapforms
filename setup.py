import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name = 'django-bootstrapforms',
    version = '0.1.0',
    packages = ['bootstrapforms'],
    include_package_data = True,
    install_requires = [],
    dependency_links = [],
    license = 'MIT License',
    description = 'Bootstrap Forms',
    long_description = README,
    url = 'http://aaronpresley.com/django-bootstrapforms',
    author = 'Aaron Presley',
    author_email = 'ap@aaronpresley.net',
)

# Django Bootstrap Forms

Django Bootstrap Forms allows you to output your Django forms in the way
Bootstrap recommends.

## Disclaimer

This project is *strongly* a work in progress and should be used cautiously.
It's one of my first Django package contributions, and I'm still figuring out
things as I go. Pull requests are welcome!

## TODO:

- I still haven't implemented all possible input types. I'm mostly creating them
as needed.
- Writing tests (I know I know...)
- Better comments

## Usage

### Install

    pip install https://bitbucket.org/AaronPresley/django-bootstrapforms/get/master.zip#egg=django-bootstrapforms-0.1.0

### Add to INSTALLED_APPS

Add `bootstrapforms` to your list of installed apps:

    INSTALLED_APPS = [
        ...
        'bootstrapforms',
    ]

### Include in Your Template

    {% include bootstrapforms %}

### Output an Entire Form

If you have an entire form you want to display, you can do it like so:

    <form method="post">
        {% csrf_token %}
        {% bsform form %}
        <button type="submit">Submit</button>
    </form>

### Output a Form Group

The `bsformgroup` tag outputs everything for a single field: the label,
the input, a help span (if the field has one), and any errors.

    <form method="post">
        {% csrf_token %}
        {% bsformgroup form.title %}
        <button type="submit">Submit</button>
    </form>

Will output:

    <form method="post">
        {% csrf_token %}
        <div class="form-group">
            <label class="control-label">Title</label>
            <input type="text" id="initial-id_title" name="title" value="" class="form-control">
            <span class="text-danger">This field is required.</span>
        </div>
        <button type="submit">Submit</button>
    </form>

### Output Form Errors

Output all of the form's non-field errors with the `bsformerror` tag:

    {% bsformerror form %}

Note that if the form doesn't have any errors, nothing will show.

### Output a Label

If you just want a label, you can call the `bslabel` tag:

    {% bslabel form.title class_string="optional-class" %}

Will give you the output of:

    <label class="control-label optional-class">Title</label>

### Output a Control

Similarly, to get a single input control, you can use the `bscontrol` tag:

    {% bscontrol form.title %}

Will output as:

    <input type="text" id="initial-id_title" name="title" value="" class="form-control">

### Output Field Errors:

To output a single field's errors, use the `bsfielderror` tag:

    {% bsfielderror form.title %}

Will give you the following:

    <span class="text-danger">Whatever error here</span>

Note that if the passed field doesn't have any errors, nothing will be output.
